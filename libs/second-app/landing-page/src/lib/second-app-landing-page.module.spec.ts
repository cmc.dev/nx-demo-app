import { async, TestBed } from '@angular/core/testing';
import { SecondAppLandingPageModule } from './second-app-landing-page.module';

describe('SecondAppLandingPageModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SecondAppLandingPageModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(SecondAppLandingPageModule).toBeDefined();
  });
});
