import { async, TestBed } from '@angular/core/testing';
import { MainHomepageModule } from './main-homepage.module';

describe('MainHomepageModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MainHomepageModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(MainHomepageModule).toBeDefined();
  });
});
