import 'zone.js/dist/zone-node';
import * as express from 'express';
import { renderModuleFactory } from '@angular/platform-server';
import { enableProdMode } from '@angular/core';
import { provideModuleMap } from '@nguniversal/module-map-ngfactory-loader';
import { join } from 'path';
import { readFileSync } from 'fs';
import { UniversalServerConfig } from './config.interface';

export function getApp (config: UniversalServerConfig<any>): any {
  enableProdMode();
  
  const template = readFileSync(join(config.clientDistPath, 'index.html')).toString();
    
  const app = express();
  
  app.get('/api', (_, res) => {
    res.send(`Welcome to universal-server!`);
  });
  
  app.engine('html', (_, options, callback) => {
    renderModuleFactory(config.moduleFactory, {
      document: template,
      url: options.req.url,
      extraProviders: [
        provideModuleMap(config.moduleMap)
      ]
    }).then(html => {
      callback(null, html);
    });
  });
  
  app.set('view engine', 'html');
  app.set('views', config.clientDistPath);
  app.get('*.*', express.static(config.clientDistPath, {
    maxAge: '1y'
  }));
  
  app.get('*', (req, res) => {
    res.render('index', { req });
  });

  return app;
}
