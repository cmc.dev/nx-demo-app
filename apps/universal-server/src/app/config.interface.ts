import { NgModuleFactory } from '@angular/core';
import { ModuleMap } from '@nguniversal/module-map-ngfactory-loader/src/module-map';

export interface UniversalServerConfig<T> {
  clientDistPath: string;
  moduleFactory: NgModuleFactory<T>;
  moduleMap: ModuleMap;
}